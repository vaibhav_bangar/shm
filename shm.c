#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/file.h>
#include <sys/mman.h>
#include <sys/wait.h>
#include "shm.h"

int shm_init(char *name,int memory_created,focas_mem_data_t **bend)
{
	const char *memname = name;
	const size_t region_size = sysconf(_SC_PAGE_SIZE);//
	int fd;
	if(!(memory_created))
	{
         fd = shm_open(memname, O_CREAT | O_TRUNC | O_RDWR, 0666);
	}
	else
	{
        fd = shm_open(memname, O_RDWR, 0666);
	}
	

	if(fd<0)
	{
		printf("error in opening shm \n");
		return 0;
	}
	
	int r;
	if(!(memory_created))
	{
		r = ftruncate(fd, region_size);
		if (r != 0)
		{
			printf("error in truncating shm \n");
			return 0;
		}
	}

  	

    *bend = (focas_mem_data_t *)mmap(0, region_size, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);

    if (*bend == MAP_FAILED)
	{
		printf("error in mmap \n");
		return 0;
	}
  	close(fd);


	if(!(memory_created))
	{
        if( sem_init(&((*bend)->bend_sem),1,1) < 0)
    	{
      		printf("semaphore initilization\n");
			return 0;
    	}
	}
    return 1;
}


int shm_deinit(char *name,focas_mem_data_t **bend)
{
	int r;
	const size_t region_size = sysconf(_SC_PAGE_SIZE);//	
    r = munmap(*bend, region_size);
	if (r != 0)
	{
        //printf("munmap \n");
		return 0;
	}

    r = shm_unlink(name);
	if (r != 0)
    {
        return 0;
    }
    return 1;
}
/********to put some data into share memory*****************/
/*
void put_data_in_shm()
{
    sem_wait(&bend->bend_sem);
    memcpy(bend->bend ,&bend_data, sizeof(bend_data_local_t));
    sem_post(&bend->bend_sem);
}

void get_data_from_shm()
{
    sem_wait(&bend->bend_sem);
    memcpy(&bend_data,bend->bend, sizeof(bend_data_local_t));
    sem_post(&bend->bend_sem);
}

int main(int argc, char *argv[]) {
	int i = 0;
	while(i<10)
	{
		printf("Wrinting\n");
		i++;
		bend_data[0].push_pos = i;		
		sem_wait(&bend->bend_sem);
		memcpy(bend->bend ,bend_data, sizeof(bend->bend));		
		sem_post(&bend->bend_sem);
		printf("Wrinting DONE \n");
		sleep(1);
	}
  return 0;
}*/

