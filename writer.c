#include <stdio.h>
#include <time.h>
#include <string.h>
#include "shm.h"
#include "global.h"

focas_mem_data_t *bend;
focas_data_local_t focas_mem_data;

void put_data_in_shm()
{
    sem_wait(&bend->bend_sem);
    memcpy(&(bend->pos_p),&focas_mem_data, sizeof(focas_data_local_t));
    sem_post(&bend->bend_sem);
}


int main(void)
{
	int pos=0;
	int size =0;
	int hypo = 0;
	int r;

    r = shm_init("sample",0,&bend);
    if(r == 0)
    {
        perror("shm init failed");
    }
	while(1)
	{
		printf("Hello \n");
		printf("ENTER THE NUMBER:");
		scanf("%d",&pos);
		printf("ENTER THE NUMBER:");
		scanf("%d",&size);
		printf("ENTER THE NUMBER:");
		scanf("%d",&hypo);
		printf("GOT pos_p=%d\n",pos);
		printf("GOT size_s=%d\n",size);
		printf("GOT hypo_h=%d\n",hypo);
		focas_mem_data.pos_p = pos;
		focas_mem_data.size_s = size;
		focas_mem_data.hypo_h = hypo;
		put_data_in_shm();
		sleep(1);
	}

    r = shm_deinit("sample",&bend);
    if(r == 0)
    {
        printf("coldnt delete SHM\n");
    }

	return 0;
}
