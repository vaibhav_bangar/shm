#include <semaphore.h>

typedef struct focas_mem_data{
	int pos_p;
	int size_s;
	int hypo_h;
	sem_t bend_sem;
}focas_mem_data_t;

typedef struct focas_data_local{
    int pos_p;
	int size_s;
	int hypo_h;
}focas_data_local_t;


int shm_init(char *name,int memory_created,focas_mem_data_t **bend);
int shm_deinit(char *name,focas_mem_data_t **bend);

