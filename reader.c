#include <stdio.h>
#include <time.h>
#include <string.h>
#include "shm.h"
#include "global_reader.h"

temp_mem_data_t *bend;
bend_data_local_t temp_mem_data;

void get_data_from_shm()
{
    sem_wait(&bend->bend_sem);
    memcpy(&temp_mem_data,&(bend->pos_p), sizeof(bend_data_local_t));
    sem_post(&bend->bend_sem);
}

int main(void)
{
	int r;
	int current_data;
	int previous_data=0;
	
    r = shm_init("sample",1,&bend);
    if(r == 0)
    {
        perror("shm init failed");
    }
	
	
	
	while(1)
	{
		get_data_from_shm();
		current_data = temp_mem_data.pos_p;
		if(current_data != previous_data){
			printf("reading pos %d \n",temp_mem_data.pos_p);
			printf("reading size %d \n",temp_mem_data.size_s);
			printf("reading hypo%d \n",temp_mem_data.hypo_h);
			
		    previous_data = current_data;
		}
		sleep(1);
	}

    r = shm_deinit("sample",&bend);
    if(r == 0)
    {
        printf("coldnt delete SHM\n");
    }

	return 0;
}